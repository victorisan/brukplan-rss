from lxml import html
import requests 

def getlink():
    page = requests.get("https://brukenthal.ro")
    tree = html.fromstring(page.content)
    link = tree.xpath("/html/body/div[1]/div/div[2]/header/div/div/div[2]/nav/ul[1]/li[5]/ul/li[1]/a")[0].get("href")
    return link

# print(type(getlink()))

