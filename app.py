from getlink import getlink
from feedgen.feed import FeedGenerator
from datetime import datetime, timezone, timedelta
import time

# Timezone
timezone_offset = +3.0  # European Standard Time (UTC+03:00)
tzinfo = timezone(timedelta(hours=timezone_offset))

# Feed Data
mylink = None
fg = FeedGenerator()
fg.title('Brukenthal Timetable')
fg.description('Updates to the Brukenthal Timetable')
fg.id('https://brukenthal.ro')
fg.link(href='https://brukenthal.ro')
fg.updated(datetime.now(tzinfo))

# New Items
def refresh():
    global fg, mylink    
    if mylink != getlink():
        mylink = getlink()
        fe = fg.add_entry()
        fe.title('New Timetable')
        fe.description('The timetable has been updated!')
        fe.link(href=mylink)
        fe.guid(mylink)
        fe.pubDate(datetime.now(tzinfo))
    fg.rss_file(pretty=True, filename="rss.xml")
    
while True:
    refresh()
    time.sleep(1800) # Refresh every 30 minutes



